<?php
//Set the error reporting, remember to change for live code
//TODO: Setup switch for
error_reporting(E_ALL | E_NOTICE);

ini_set('display_errors', 1);

ini_set('date.timezone', "Europe/London");
$start = microtime();
ob_start();
session_start();

require_once('system/library/loader.php');

$loader = new loader();

$loader->loadCore(array('frontController', 'routing'));


echo("\n<!-- Loaded in " . (microtime() - $start) . " seconds-->");
